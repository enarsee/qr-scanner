package com.enarsee.qrscanner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class MyActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
    }

    public void scanStart(View view) {
        Intent intent = new Intent(this, ScannerActivity.class);
        startActivityForResult(intent,0);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("result");

                getDetails(contents);

            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }

    public void getDetails(String content)
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        String link = prefs.getString("backendLink",null);



        String serverIp = link+"?code="+content;

        CallAPI async = new CallAPI(this);
        async.execute(serverIp);

    }

    private class CallAPI extends AsyncTask<String, String, String> {

        private Context mContext;
        public CallAPI (Context context){
            mContext = context;
        }

        ProgressDialog pd;

        @Override
        protected String doInBackground(String... params) {

            String urlString=params[0];

            String resultToDisplay = "";

            InputStream is = null;
            String result = "";


            // HTTP
            try {
                HttpClient httpclient = new DefaultHttpClient(); // for port 80 requests!
                HttpGet httppost = new HttpGet(urlString);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
            } catch(Exception e) {
                return null;
            }

            // Read response to string
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"),8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
                displayMessage d = new displayMessage();
                d.message = result;
                MyActivity.this.runOnUiThread(d);
            } catch(Exception e) {
                return null;
            }
            return "";
        }



        protected void onPreExecute()
        {
            pd= ProgressDialog.show(MyActivity.this, "", "Please Wait", false);
        }
        protected void onPostExecute(String result) {
            pd.dismiss();





        }

    }
    private class displayMessage implements Runnable{

        public String message;
        @Override
        public void run() {

            TextView result = (TextView)findViewById(R.id.result);
            result.setText(Html.fromHtml(message));


        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
